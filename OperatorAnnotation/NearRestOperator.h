//
//  NearRestDriv.h
//  Unicabi
//
//  Created by AITL on 12/14/13.
//
//

#import <Foundation/Foundation.h>

@interface NearRestOperator : NSObject<MKAnnotation>

@property(nonatomic,strong)NSDictionary *InfoDictionary;
@property(nonatomic)int annotationTag;

- (id)initWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude;
-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;


@end
