//
//  Mappoint.h
//  CabProject Customer Latest
//
//  Created by AITL BD on 17/01/2013.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>



@interface OperatorAnn : NSObject<MKAnnotation>{
    NSString *_name;
    NSString *_address;
    CLLocationCoordinate2D _coordinate;
}
@property (copy)NSString *name;
@property (copy)NSString *address;
@property (nonatomic,readonly)CLLocationCoordinate2D coordinate;

-(id)initWithName:(NSString *)name address:(NSString *)address coordinate :(CLLocationCoordinate2D)coordinate;
@end
