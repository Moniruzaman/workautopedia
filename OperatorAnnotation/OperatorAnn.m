//
//  Mappoint.m
//  CabProject Customer Latest
//
//  Created by AITL BD on 17/01/2013.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "OperatorAnn.h"

@implementation OperatorAnn
@synthesize name = _name;
@synthesize address = _address;
@synthesize coordinate = _coordinate;

-(id)initWithName:(NSString *)name address:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate{
    if(self = [super init]){
        _name = name;
        _address = address;
        _coordinate = coordinate;
    }
    return self;
}
-(NSString *)title{
    return _name;
}
-(NSString *)subtitle{
    return _address;
}
@end
