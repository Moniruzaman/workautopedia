//
//  NearRestDriv.m
//  Unicabi
//
//  Created by AITL on 12/14/13.
//
//

#import "NearRestOperator.h"
@interface NearRestOperator()

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;


@end

@implementation NearRestOperator
@synthesize InfoDictionary;

- (id)initWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude
{
	self = [super init];
	if (self)
	{
		self.latitude = latitude;
		self.longitude = longitude;
	}
	return self;
}
- (CLLocationCoordinate2D)coordinate
{
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = self.latitude;
	coordinate.longitude = self.longitude;
	return coordinate;
}
-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{

}
@end
