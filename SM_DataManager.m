//
//  SM_DataManager.m
//  SM_101_BanglaIslamiceEBook
//
//  Created by Majharul Huq on 11/28/14.
//  Copyright (c) 2014 SmartMux. All rights reserved.
//

#import "SM_DataManager.h"


@implementation SM_DataManager

+ (SM_DataManager *)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject                        = nil;
    //__strong static KoukaibukoroDataManager *_dataManager   = nil;
    
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        
    });
    
    // returns the same object each time
    return _sharedObject;
}
- (void)removeDatafiles
{
    NSString *path=[NSString stringWithFormat:@"%@/",FILESAVE_PATH];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:path error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", path, file] error:&error];
        if (!success || error) {
            NSLog(@"ERROR : File Not Removed");
        }
    }

}
-(BOOL)checkDateNeedToReplaceFile:(NSString *)filename
{
    NSString *path=[NSString stringWithFormat:@"%@/%@.plist",FILESAVE_PATH,filename];
    if(![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
    {
        NSLog(@"ERROR : File Not Exist");
        return NO;
    }
    NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
    NSDate *result = [fileAttribs objectForKey:NSFileCreationDate]; //or NSFileModificationDate
    NSLog(@"%@",result);
    
   
    NSDate *startDate = [NSDate date];
    NSLog(@"%@",startDate);
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:result
                                                         options:0];
    if (components.day == 7 ) {
        return YES;
    }
    return NO;
}
- (BOOL)checkDataFile:(NSString*)filename
{
    NSString *path=[NSString stringWithFormat:@"%@/%@.plist",FILESAVE_PATH,filename];
    if(![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
    {
        NSLog(@"ERROR : File Not Exist");
        return NO;
    }
    return YES;
}
- (void)saveData:(NSDictionary*)data :(NSString *)filename
{
    NSString *path=[NSString stringWithFormat:@"%@/%@.plist",FILESAVE_PATH,filename];
    [data writeToFile:path atomically:YES] ? NSLog(@"Data Has Been Write Successfully") : NSLog(@"Data  Write Failure");
}
- (NSArray *)readData:(NSString *)filename
{
    NSString *path=[NSString stringWithFormat:@"%@/%@.plist",FILESAVE_PATH,filename];
    NSArray      *arrData = [[NSArray alloc] initWithContentsOfFile:path];
    return arrData;

}

@end
