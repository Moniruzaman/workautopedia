//
//  ExampleViewController.h
//  LeavesExamples
//
//  Created by Tom Brow on 4/18/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

#import "LeavesViewController.h"

@interface ImageExampleViewController : LeavesViewController
{

}
- (id)init:(NSArray *)imageary;
@property(nonatomic,retain)NSDictionary *datadic;
@property(nonatomic,retain)NSArray *ImageDataArray;

@end
