//
//  ParentViewConteoller.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 3/23/15.
//  Copyright (c) 2015 Rasel_cse07. All rights reserved.
//

#import "ParentViewConteoller.h"

@interface ParentViewConteoller ()

@end

@implementation ParentViewConteoller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    navToolbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    navToolbar.backgroundColor = [UIColor whiteColor];
    navToolbar.alpha = 0.8;
    [self.view addSubview:navToolbar];
    
    UIButton *backbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 150, 40)];
    [backbtn setImage:[UIImage imageNamed:@"backImage.png"] forState:UIControlStateNormal];
    [backbtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *myBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backbtn];
    
    //UIBarButtonItem *myBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(backAction:)];

    
    UINavigationItem *right = [[UINavigationItem alloc] initWithTitle:@""];
    right.leftBarButtonItem = myBarButtonItem;
    
    [navToolbar pushNavigationItem:right animated:YES];

}

-(IBAction)backAction:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
