//
//  web_ViewController.h
//  Bizluk
//
//  Created by Rasel_cse07 on 11/20/13.
//  Copyright (c) 2013 Rasel_cse07. All rights reserved.
//

#import "ViewController.h"


@interface web_ViewController : UIViewController<UIWebViewDelegate, UITextFieldDelegate, UIActionSheetDelegate>
{

}




@property (nonatomic, readonly) IBOutlet UIWebView *webView;

@property (nonatomic, retain) IBOutlet UIView *titleToolbar;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, retain) IBOutlet UIToolbar *addressToolbar;
@property (nonatomic, retain) IBOutlet UITextField *urlField;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;

@property (nonatomic, getter = isTitleBarVisible) BOOL showTitleBar;
@property (nonatomic, getter = isAddressBarVisible) BOOL showAddressBar;
@property (nonatomic, getter = isToolbarVisible) BOOL showToolbar;
@property (nonatomic) BOOL showDoneButton;

@property (nonatomic, retain) NSURL *url;
@property (nonatomic, retain) NSString *html;
@property (nonatomic, retain) NSData *data;
@property (nonatomic, retain) NSString *formSourceView;


#pragma mark - UIToolbar (TTCategory)
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

@end
@interface UIToolbar (web_ViewController)

- (void)replaceItem:(UIBarButtonItem *)oldItem withItem:(UIBarButtonItem *)item;

@end
