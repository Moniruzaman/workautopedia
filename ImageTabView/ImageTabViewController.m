//
//  ImageTabViewController.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 8/30/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "ImageTabViewController.h"
#import "ImageCell.h"
#import "UIImageView+AFNetworking.h"
#import "imageZoomViewController.h"
#import "imageZoomViewController.h"
#import "ImageExampleViewController.h"
#import "MBProgressHUD.h"

@interface ImageTabViewController ()

@end

@implementation ImageTabViewController
@synthesize datadic;
@synthesize ImageDataArray;
@synthesize ImageArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title=@"Images";
        self.tabBarItem.image           = [[UIImage imageNamed:@"tab_imageN.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.tabBarItem.selectedImage   = [[UIImage imageNamed:@"tab_imageH.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    ImageDataArray=[[NSArray alloc] init];
    ImageArray=[[NSMutableArray alloc] init];
    tableView1.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-49);
    tableView1.contentInset = UIEdgeInsetsMake(44, 0, 0, 0);
    photos = [[NSMutableArray alloc] init];
    thumbs = [[NSMutableArray alloc] init];
    [self jsonTapped];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table View Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ImageDataArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ImageCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    NSURL *url = [NSURL URLWithString:[[ImageDataArray objectAtIndex:indexPath.row] valueForKey:@"tbUrl"]];
   // NSData *data = [NSData dataWithContentsOfURL:url];
    //[cell.image_view setImage:[UIImage imageWithData:data]];
    [cell.image_view setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    [cell.formatedLabelString setText:[[ImageDataArray objectAtIndex:indexPath.row] valueForKey:@"titleNoFormatting"]];//contentNoFormatting
    return cell;
}
- (void)tableView:(UITableView *)table didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
  [self loadPhotoListView];
    /*
   ImageExampleViewController *viewController  = [[ImageExampleViewController alloc] init:ImageArray];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
   [self presentViewController:nav animated:YES completion:nil];
    */
}
/*
- (void)loadImage:(NSString *)url {
    NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    [self performSelectorOnMainThread:@selector(displayImage:) withObject:image waitUntilDone:NO];
}
- (void)displayImage:(UIImage *)image {
    ImageCell *cell = [tableView1 dequeueReusableCellWithIdentifier:@"ImageCell"];
    [cell.image_view setImage:image]; //UIImageView
}
*/
- (void)jsonTapped
{
   [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // 1
    NSString *string = [NSString stringWithFormat:@"https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=%@%@%@&rsz=8&key=AIzaSyDeLi0UbChPZ6OT6vbjm6XeXnlSrd5zeKU",[[[SingleTon instance] searchStringAry] objectAtIndex:2],[[[SingleTon instance] searchStringAry] objectAtIndex:0],[[[SingleTon instance] searchStringAry] objectAtIndex:1]];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 2
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // 3
        self.datadic = (NSDictionary *)responseObject;
        NSLog(@"DataValue--->%@",datadic);
        ImageDataArray=[[datadic objectForKey:@"responseData"]objectForKey:@"results"];
        //NSLog(@"Imagurl--->%@",[[ImageDataArray objectAtIndex:1] valueForKey:@"url"]);
        
        for (int i=0; i<[ImageDataArray count]; i++) {
            
            /*
            NSURL *url = [NSURL URLWithString:[[ImageDataArray objectAtIndex:i] valueForKey:@"unescapedUrl"]];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *img = [[UIImage alloc]initWithData:data];
            [ImageArray addObject:img];
            */
            // Photos
            
            [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[ImageDataArray objectAtIndex:i] valueForKey:@"unescapedUrl"]]]];
            // Thumbs
            [thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[ImageDataArray objectAtIndex:i] valueForKey:@"tbUrl"]]]];
            
           // break;
        }
        
        [tableView1 reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // 4
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
    
    // 5
    [operation start];
}
-(void)loadPhotoListView
{
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = NO;
    BOOL displayNavArrows = NO;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    //MWPhoto *photo;
    
    self.photos = photos;
    self.thumbs = thumbs;
    // Create browser
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = displayActionButton;
    browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = displaySelectionButtons;
    browser.zoomPhotosToFill = YES;
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
     browser.wantsFullScreenLayout = YES;
#endif
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = YES;
    [browser setCurrentPhotoIndex:0];
    
    // Reset selections
    if (displaySelectionButtons) {
         _selections = [NSMutableArray new];
        for (int i = 0; i < photos.count; i++) {
            [_selections addObject:[NSNumber numberWithBool:NO]];
             }
        }
    
    //[self.navigationController pushViewController:browser animated:YES];
    
         UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
         nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
         [self presentViewController:nc animated:YES completion:nil];
    
    
     // Test reloading of data after delay
     double delayInSeconds = 3;
     dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
      });

}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
     return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
     if (index < _photos.count)
         return [_photos objectAtIndex:index];
     return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
     return [_thumbs objectAtIndex:index];
     return nil;
}

//- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
//    MWPhoto *photo = [self.photos objectAtIndex:index];
//    MWCaptionView *captionView = [[MWCaptionView alloc] initWithPhoto:photo];
//    return [captionView autorelease];
//}

//- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"ACTION!");
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
     NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
     return [[_selections objectAtIndex:index] boolValue];
}

//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index {
//    return [NSString stringWithFormat:@"Photo %lu", (unsigned long)index+1];
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    [_selections replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:selected]];
     NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
     // If we subscribe to this method we must dismiss the view controller ourselves
     NSLog(@"Did finish modal presentation");
     [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
