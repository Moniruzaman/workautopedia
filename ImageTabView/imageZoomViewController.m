//
//  imageZoomViewController.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 12/29/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "imageZoomViewController.h"

@interface imageZoomViewController ()

@end

@implementation imageZoomViewController
@synthesize urlText;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"urlText---->%@",urlText);
    [myWebView  loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlText]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dissmissAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
