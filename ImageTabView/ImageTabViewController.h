//
//  ImageTabViewController.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 8/30/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "JSON.h"
#import "SBJSON.h"
#import "ParentViewConteoller.h"
#import "MWPhoto.h"
#import "MWPhotoBrowser.h"


@interface ImageTabViewController : ParentViewConteoller<UITableViewDelegate,UITableViewDataSource,MWPhotoBrowserDelegate>
{
    __weak IBOutlet UITableView *tableView1;
    NSMutableArray *_selections;
    NSMutableArray *photos;
    NSMutableArray *thumbs;
}
@property(nonatomic,retain)NSDictionary *datadic;
@property(nonatomic,retain)NSArray *ImageDataArray;
@property(nonatomic,retain)NSMutableArray *ImageArray;


@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *thumbs;

@end
