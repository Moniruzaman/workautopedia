//
//  ImageCell.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 8/30/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCell : UITableViewCell
{
}
@property (weak, nonatomic) IBOutlet UIImageView *image_view;
@property (weak, nonatomic) IBOutlet UILabel *formatedLabelString;

@end
