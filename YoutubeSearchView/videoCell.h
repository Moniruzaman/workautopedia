//
//  videoCell.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 9/4/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface videoCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView_video;
@property (weak, nonatomic) IBOutlet UILabel *description_label;
@property (weak, nonatomic) IBOutlet UILabel *title_label;

@end
