//
//  youtubeSearchView.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 9/4/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "youtubeSearchView.h"
#import "videoCell.h"
#import "web_ViewController.h"
#import "UIImageView+AFNetworking.h"

@interface youtubeSearchView ()

@end

@implementation youtubeSearchView
@synthesize datadic;
@synthesize ShowroomeAry;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title=@"Video Reviews";
        self.tabBarItem.image           = [[UIImage imageNamed:@"tab_youtubeN.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.tabBarItem.selectedImage   = [[UIImage imageNamed:@"tab_youtubeH.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    maintableoutlet.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-49);
    maintableoutlet.contentInset = UIEdgeInsetsMake(44, 0, 0, 0);
    [self jsonTapped];
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlAddress]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table View Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ShowroomeAry count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    videoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videoCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"videoCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    
    NSLog(@"default--->%@",[[[[[ShowroomeAry objectAtIndex:indexPath.row] objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"default"] valueForKey:@"url"]);
    NSURL *url = [NSURL URLWithString:[[[[[ShowroomeAry objectAtIndex:indexPath.row] objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"default"] valueForKey:@"url"]];
    [cell.imageView_video setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    
    cell.description_label.text=[[[ShowroomeAry objectAtIndex:indexPath.row] objectForKey:@"snippet"] valueForKey:@"description"];
    cell.title_label.text=[[[ShowroomeAry objectAtIndex:indexPath.row] objectForKey:@"snippet"] valueForKey:@"title"];
    
    
    return cell;
}
- (void)tableView:(UITableView *)table didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* youtubeURL = [NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",[[[ShowroomeAry objectAtIndex:indexPath.row] objectForKey:@"id"] valueForKey:@"videoId"]];
    [self play_btn_1:youtubeURL];
    //[self playMovie:self];
    [table deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)play_btn_1:(NSString *)youtubeURL
{
   
    
    @try {
       /*
        NSURL *videosURL = [NSURL URLWithString:youtubeURL];
        MPMoviePlayerController *moviePlayController = [[MPMoviePlayerController alloc]initWithContentURL:videosURL];
        [self.view addSubview:moviePlayController.view];
        
        [moviePlayController prepareToPlay];
        moviePlayController.controlStyle = MPMovieControlStyleDefault;
        moviePlayController.shouldAutoplay = YES;
        [moviePlayController setFullscreen:YES animated:YES];
        */
        
        web_ViewController *web;
        if ([UIScreen mainScreen].bounds.size.height!=568) {
            
            web=[[web_ViewController alloc] initWithNibName:@"web_ViewController_3" bundle:nil];
        }
        else
        {
            web=[[web_ViewController alloc] initWithNibName:@"web_ViewController" bundle:nil];
        }
        
        
                web.showDoneButton = YES;
                web.url=[NSURL URLWithString:youtubeURL];
                [self presentViewController:web animated:YES completion:nil];
        
    }
    
    @catch (NSException *exception) {
        NSLog(@" exc: %@",exception);
    }
    @finally {
        
    }
    //http://www.youtube.com/watch?v=znhutEkK9Tw
}
-(void)playMovie:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://www.youtube.com/watch?v=znhutEkK9Tw"
                  ];//@"http://www.ebookfrenzy.com/ios_book/movie/movie.mov"
    
    _moviePlayer1 =  [[MPMoviePlayerController alloc]
                     initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayer1];
    
    _moviePlayer1.controlStyle = MPMovieControlStyleDefault;
    _moviePlayer1.shouldAutoplay = YES;
    [self.view addSubview:_moviePlayer1.view];
    [_moviePlayer1 setFullscreen:YES animated:YES];
}
//- (void) moviePlayBackDidFinish:(NSNotification*)notification {
//    MPMoviePlayerController *player = [notification object];
//    [[NSNotificationCenter defaultCenter]
//     removeObserver:self
//     name:MPMoviePlayerPlaybackDidFinishNotification
//     object:player];
//    
//    if ([player
//         respondsToSelector:@selector(setFullscreen:animated:)])
//    {
//        [player.view removeFromSuperview];
//    }
//}
- (void)jsonTapped
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // 1
    NSString *string=[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=8&q=%@%@%@+Reviews&key=AIzaSyDyjfbdHAm8a1p3lvflaoFueG4CCG5-I7s",[[[SingleTon instance] searchStringAry] objectAtIndex:2],[[[SingleTon instance] searchStringAry] objectAtIndex:0],[[[SingleTon instance] searchStringAry] objectAtIndex:1]];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 2
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // 3
        self.datadic = (NSDictionary *)responseObject;
        NSLog(@"DataValue--->%@",datadic);
        ShowroomeAry=[datadic objectForKey:@"items"];
        NSLog(@"ShowroomeAry--->%@",ShowroomeAry);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [maintableoutlet reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // 4
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
    
    // 5
    [operation start];
}
@end
