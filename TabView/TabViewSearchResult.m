//
//  TabViewSearchResult.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 8/29/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "TabViewSearchResult.h"


@interface TabViewSearchResult ()

@end

@implementation TabViewSearchResult

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Add it to the navigation bar
    self.navigationController.navigationBarHidden = YES;
    
    //create the view controller for the tabs
    //self.web_ViewControllerInstance  = [[web_ViewController alloc] initWithNibName:@"web_ViewController" bundle:[NSBundle mainBundle]];
    //[self.web_ViewControllerInstance setFormSourceView:@"TabView"];
    
    self.webResultViewControllerInstance = [[webResultViewController alloc] initWithNibName:@"webResultViewController" bundle:[NSBundle mainBundle]];
    self.youtubeSearchViewInstance   = [[youtubeSearchView alloc] initWithNibName:@"youtubeSearchView" bundle:[NSBundle mainBundle]];
    self.ImageTabViewControllerInstance = [[ImageTabViewController alloc] initWithNibName:@"ImageTabViewController" bundle:[NSBundle mainBundle]];
    self.MapAnnotationPlotViewInstance = [[MapAnnotationPlotView alloc] initWithNibName:@"MapAnnotationPlotView" bundle:[NSBundle mainBundle]];
    
    
    //create an array of all view controllers that will represent the tab at the bottom
    NSArray *myViewControllers = [[NSArray alloc] initWithObjects:
                                  self.webResultViewControllerInstance,//web_ViewControllerInstance
                                  self.youtubeSearchViewInstance,
                                  self.ImageTabViewControllerInstance,//ImageTabViewControllerInstance
                                  self.MapAnnotationPlotViewInstance,
                                  nil];
    
    
    self.tabBar.backgroundColor = [UIColor clearColor];
    
    //UIImage* tabBarBackground = [UIImage imageNamed:@"Koukaibukoro.bundle/tab_background"];
    //[[UITabBar appearance] setBackgroundImage:tabBarBackground];
    
    //set the tab bar title appearance for normal state
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:11.0f],
                                                        NSForegroundColorAttributeName : [UIColor blackColor]
                                                        } forState:UIControlStateNormal];
    
    
    //set the tab bar title appearance for selected state
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:11.0f],
                                                        NSForegroundColorAttributeName : [[AppDelegate ShareDelegate] colorFromHexString:@"b74800"]
                                                        } forState:UIControlStateSelected];
    
    //set the view controllers for the tab bar controller
    [self setViewControllers:myViewControllers];
    
    //self.navigationItem.titleView = segmentedControl;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
