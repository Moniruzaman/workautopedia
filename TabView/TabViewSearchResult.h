//
//  TabViewSearchResult.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 8/29/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "web_ViewController.h"
#import "ImageTabViewController.h"
#import "MapAnnotationPlotView.h"
#import "youtubeSearchView.h"
#import "webResultViewController.h"

@interface TabViewSearchResult : UITabBarController
{
    
    
}


@property (strong, nonatomic) web_ViewController        *web_ViewControllerInstance;
@property (strong, nonatomic) youtubeSearchView         *youtubeSearchViewInstance;
@property (strong, nonatomic) ImageTabViewController    *ImageTabViewControllerInstance;
@property (strong, nonatomic) MapAnnotationPlotView     *MapAnnotationPlotViewInstance;
@property (strong, nonatomic) webResultViewController   *webResultViewControllerInstance;

@end
