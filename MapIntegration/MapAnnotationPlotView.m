//
//  MapAnnotationPlotView.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 9/2/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "MapAnnotationPlotView.h"
#import "NearRestOperator.h"

@interface MapAnnotationPlotView ()

@end

@implementation MapAnnotationPlotView
@synthesize datadic;
@synthesize ShowroomeAry;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title=@"Dealership";
        self.tabBarItem.image           = [[UIImage imageNamed:@"tab_delarN.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.tabBarItem.selectedImage   = [[UIImage imageNamed:@"tab_delarH.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self jsonTapped];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)jsonTapped
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // 1
    NSString *string=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/textsearch/json?location=%@,%@&radius=5000&query=%@&sensor=true&key=AIzaSyDyjfbdHAm8a1p3lvflaoFueG4CCG5-I7s",[[SingleTon instance] CurrentLocationlat],[[SingleTon instance] CurrentLocationlon],[[[SingleTon instance] searchStringAry] objectAtIndex:0]];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 2
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // 3
        self.datadic = (NSDictionary *)responseObject;
        NSLog(@"DataValue--->%@",datadic);
        ShowroomeAry=[datadic objectForKey:@"results"];
        NSLog(@"ShowroomeAry--->%@",ShowroomeAry);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        for (int i=0; i<[ShowroomeAry count]; i++) {
            
            CLLocationCoordinate2D coordinate;
            coordinate.latitude         = [[[[[ShowroomeAry objectAtIndex:i] objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lat"] floatValue];
            coordinate.longitude        = [[[[[ShowroomeAry objectAtIndex:i] objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lng"] floatValue];
            MKCoordinateSpan span;
            span.latitudeDelta          = 1;
            span.longitudeDelta         = 1;
            MKCoordinateRegion region   = {coordinate, span};
            [MainMapView setRegion:region];
            
            MKPointAnnotation *poi = [[MKPointAnnotation alloc] init];
            poi.coordinate = coordinate;
            poi.title       = [[ShowroomeAry objectAtIndex:i] valueForKey:@"name"];
            poi.subtitle    = [[ShowroomeAry objectAtIndex:i] valueForKey:@"formatted_address"];
            [MainMapView addAnnotation:poi];
            
            /*
            NSLog(@"%f",[[[[[ShowroomeAry objectAtIndex:i] objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lat"] floatValue]);
            NearRestOperator *nearDrivAnn=[[NearRestOperator alloc]initWithLatitude:[[[[[ShowroomeAry objectAtIndex:i] objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lat"] floatValue] andLongitude:[[[[[ShowroomeAry objectAtIndex:i] objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lng"] floatValue]];
            nearDrivAnn.annotationTag=i;
            nearDrivAnn.InfoDictionary=[ShowroomeAry objectAtIndex:i];
            [MainMapView addAnnotation:nearDrivAnn];
            */
        }
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([[SingleTon instance] customerCurrentLocationCoordinate] ,80000,80000);
        [MainMapView setRegion:region animated:YES];
    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // 4
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
    
    // 5
    [operation start];
}
#pragma -mark MAPVIEW DELEGATE

- (void)mapView:(MKMapView *)mv didUpdateUserLocation:(MKUserLocation *)userLocation {
}
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id)annotation{
   
    if ([[annotation title] isEqualToString:@"Current Location"]) {
        return nil;
    }
    
    MKAnnotationView *annView = [[MKAnnotationView alloc ] initWithAnnotation:annotation reuseIdentifier:@"RestaurentPoi"];
    annView.image = [ UIImage imageNamed:@"Cab-red.png" ];
    annView.canShowCallout = YES;
    return annView;
    /*
    MKAnnotationView *annotationView=nil;
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        
        return nil;
    }

    // MKAnnotationView* annotationView = nil;
    if ([annotation isKindOfClass:[NearRestOperator class]])
    {
        //NearRestOperator* annotation2 = (NearRestOperator*)annotation;
        
        static NSString *identifier = @"MyView";
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        NSString *imgName=nil;
        imgName=@"Cab-red";
        //[[annotation2.InfoDictionary valueForKey:@"vehicleType"] isEqualToString:@"operator"]
        
        NSString* path = [[NSBundle mainBundle] pathForResource:imgName ofType:@"png"];
        UIImage* im = [[UIImage alloc] initWithContentsOfFile:path];
        annotationView.contentMode=UIViewContentModeScaleAspectFit;
        annotationView.image= im;//you can set your image here.
        annotationView.enabled = YES;
        annotationView.canShowCallout = NO;
        //delete the flagged Locations
    }
    
    return annotationView;
    */
}

- (IBAction)userCurrentLocation:(id)sender {
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([[SingleTon instance] customerCurrentLocationCoordinate] ,5000,5000);
    [MainMapView setRegion:region animated:YES];
    
}

@end
