//
//  MapAnnotationPlotView.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 9/2/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ParentViewConteoller.h"

@interface MapAnnotationPlotView : ParentViewConteoller<CLLocationManagerDelegate,MKMapViewDelegate>
{
    __weak IBOutlet MKMapView *MainMapView;
}
@property(nonatomic,retain)NSDictionary *datadic;
@property(nonatomic,retain)NSArray *ShowroomeAry;
- (IBAction)userCurrentLocation:(id)sender;

@end
