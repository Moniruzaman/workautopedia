//
//  SingleTon.h
//  CabProject Customer Latest
//
//  Created by AITL BD on 03/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef enum requestType{
    GET_YEAR_ACTION = 0,
    GET_CAR_MAKE_ACTION,
    GET_CAR_RES_MODEL
    
}requestType;

//its without comment 
@interface SingleTon : NSObject {

}
@property (assign) CLLocationCoordinate2D customerCurrentLocationCoordinate;
@property(nonatomic,strong)NSArray *makedataarray;
@property(nonatomic,strong)NSArray *modeldataarray;
@property(nonatomic,strong)NSArray *dateArray;

@property(nonatomic,strong)NSString *CurrentLocationlat;
@property(nonatomic,strong)NSString *CurrentLocationlon;
@property(nonatomic,retain)NSMutableArray * searchStringAry;

+ (SingleTon *) instance;

@end
