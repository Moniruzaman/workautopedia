//
//  CarMakeModelData.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 7/15/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ResponseDelegateProtocol <NSObject>

-(void)dataAfterRequest:(id)res:(int)type;

@end


@interface CarMakeModelData : NSObject
{
}
@property(nonatomic,strong)id <ResponseDelegateProtocol> delegate;
-(void)requestServerToGetCarData:(NSDictionary *)dic :(int)requestType;

@end
