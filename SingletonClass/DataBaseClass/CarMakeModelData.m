//
//  CarMakeModelData.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 7/15/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "CarMakeModelData.h"

@implementation CarMakeModelData
@synthesize delegate;

-(void)requestServerToGetCarData:(NSDictionary *)dic :(int)requestType
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"http://autopediapro.com/search.php" parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        NSDictionary *dic=[responseObject valueForKey:@"data"];
        NSString *success=[dic valueForKey:@"success"];
        if ([success isEqualToString:@"1"])
        {
            switch (requestType) {
                case GET_YEAR_ACTION:
                case GET_CAR_MAKE_ACTION:
                case GET_CAR_RES_MODEL:
                {
                    NSArray *ary=[dic valueForKey:@"year"];
                    if ([ary isEqual:@""]) {
                        [[AppDelegate ShareDelegate] customAlert:@"Info!" withMessage:@"No Respective Data Found!!!!"];
                         return;
                    }
                   //NSLog(@"JSON: %@", [ary objectAtIndex:0]);
                    [self.delegate dataAfterRequest:ary :requestType];
                    
                }break;
                default:
                    break;
            }
            
            
            
        }
       
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}
@end
