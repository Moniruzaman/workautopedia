//
//  SM_DataManager.h
//  SM_101_BanglaIslamiceEBook
//
//  Created by Majharul Huq on 11/28/14.
//  Copyright (c) 2014 SmartMux. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SM_DataManager : NSObject

+ (SM_DataManager *)sharedInstance;


- (BOOL)checkDataFile:(NSString*)filename;
- (void)saveData:(NSDictionary*)data :(NSString *)filename;
- (NSArray *)readData:(NSString *)filename;
-(BOOL)checkDateNeedToReplaceFile:(NSString *)filename;
- (void)removeDatafiles;

@end
