//
//  AppDelegate.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 7/15/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{

}

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,retain)NSDictionary *datadic;
@property(nonatomic,retain)NSDictionary *datadicformodel;
+(AppDelegate *)ShareDelegate;
- (UIColor *) colorFromHexString:(NSString *)hexString;
- (void)customAlert:(NSString*)title1 withMessage:(NSString*)message;

@end
