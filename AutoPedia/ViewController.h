//
//  ViewController.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 7/15/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CarMakeModelData.h"
#import "SM_DataManager.h"

@import GoogleMobileAds;

@interface ViewController : UIViewController<CLLocationManagerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,ResponseDelegateProtocol>
{
    IBOutlet UITextField *txt;
    __weak IBOutlet UITextField *model_text;
    __weak IBOutlet UITextField *year_txt;
    __weak IBOutlet UIButton *search_btn_outlet;
     CLLocationManager *locationManager;
    
    CarMakeModelData *carmodelDataInstance;
    SM_DataManager *datamanagerInstance;
}
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;
- (IBAction)search_btn_action:(id)sender;
@end
