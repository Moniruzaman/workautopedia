//
//  ViewController.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 7/15/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "CarMakeModelData.h"
#import "TabViewSearchResult.h"
#import "SingleTon.h"


@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBarHidden=YES;
    //self.title = @"AutoPedia";
    
    for (UITextField *txtfld in [self.view subviews]) {
        if ([txtfld isKindOfClass:[UITextField class]]) {
            [txtfld setDelegate:self];
        }
    }
    
 
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self.bannerView];
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    self.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[GADRequest request]];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
 
    self.bannerView.frame = CGRectMake(0, self.view.frame.size.height - 2*self.bannerView.frame.size.height, self.view.frame.size.width, 49);

}

-(void)viewWillAppear:(BOOL)animated
{
    year_txt.text=@"";
    txt.text=@"";
    model_text.text=@"";
    txt.userInteractionEnabled=NO;
    model_text.userInteractionEnabled=NO;
    [search_btn_outlet.layer setCornerRadius:3.0];
    [search_btn_outlet setClipsToBounds:YES];
    [search_btn_outlet setShowsTouchWhenHighlighted:YES];
    [search_btn_outlet setBackgroundColor:[UIColor darkGrayColor]];
    
    carmodelDataInstance=[[CarMakeModelData alloc] init];
    carmodelDataInstance.delegate=self;
    datamanagerInstance = [[SM_DataManager alloc] init];
    
    self.bannerView.frame = CGRectMake(0, self.view.frame.size.height-self.bannerView.frame.size.height, self.view.frame.size.width, 49);
    
    /*
    if ([datamanagerInstance checkDateNeedToReplaceFile:FILE_YEAR]) {
        
         [datamanagerInstance removeDatafiles];
         NSDictionary *dic=@{@"action" : @"getyear"};
        [carmodelDataInstance requestServerToGetCarData:dic :GET_YEAR_ACTION];
    }
    else
    {
        if ([datamanagerInstance checkDataFile:FILE_YEAR]) {
            [[SingleTon instance] setDateArray:[datamanagerInstance readData:FILE_YEAR]];
        }
        else
        {
     */
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *dic=@{@"action" : @"getyear"};
        [carmodelDataInstance requestServerToGetCarData:dic :GET_YEAR_ACTION];
    
    //}
    //}
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    // checking location is updated or not. Only the view loading time the map moved & zoomed to current location
    
    [[SingleTon instance] setCustomerCurrentLocationCoordinate:newLocation.coordinate];
    
    [[SingleTon instance] setCurrentLocationlat:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude]];
    [[SingleTon instance] setCurrentLocationlon:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude]];
    
    NSLog(@"lat%@",[[SingleTon instance] CurrentLocationlat]);
    NSLog(@"lon%@",[[SingleTon instance] CurrentLocationlon]);
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField tag]==0)
    {
        UIPickerView *cityPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        cityPicker.delegate = self;
        cityPicker.dataSource = self;
        cityPicker.tag=[textField tag];
        [cityPicker setShowsSelectionIndicator:YES];
        txt.inputView = cityPicker;
        
        
        // Create done button in UIPickerView
        UIToolbar*  mypickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 56)];
        mypickerToolbar.barStyle = UIBarStyleDefault;
        [mypickerToolbar sizeToFit];
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        
        UIButton *selectButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
        [selectButton setTitle:@"Select" forState:UIControlStateNormal];
        [selectButton setShowsTouchWhenHighlighted:YES];
        [selectButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [selectButton addTarget:self action:@selector(pickerDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
        selectButton.tag=[textField tag];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithCustomView:selectButton];
        /*
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked:)];
         */
        [barItems addObject:doneBtn];
        //doneBtn.tag=[textField tag];
        [mypickerToolbar setItems:barItems animated:YES];
        txt.inputAccessoryView = mypickerToolbar;
    }
    else if ([textField tag]==1)
    {
    
        UIPickerView *cityPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        cityPicker.delegate = self;
        cityPicker.dataSource = self;
        cityPicker.tag=[textField tag];
        [cityPicker setShowsSelectionIndicator:YES];
        model_text.inputView = cityPicker;
        
        // Create done button in UIPickerView
        UIToolbar*  mypickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 56)];
        mypickerToolbar.barStyle = UIBarStyleDefault;
        [mypickerToolbar sizeToFit];
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        
        UIButton *selectButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
        [selectButton setTitle:@"Select" forState:UIControlStateNormal];
        [selectButton setShowsTouchWhenHighlighted:YES];
        [selectButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [selectButton addTarget:self action:@selector(pickerDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
        selectButton.tag=[textField tag];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithCustomView:selectButton];
        
        /*
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(pickerDoneClicked:)];
         */
        
        [barItems addObject:doneBtn];
        //doneBtn.tag=[textField tag];
        [mypickerToolbar setItems:barItems animated:YES];
        model_text.inputAccessoryView = mypickerToolbar;
    
    
    }
    else if ([textField tag]==2)
    {
        
        UIPickerView *cityPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        cityPicker.delegate = self;
        cityPicker.dataSource = self;
        cityPicker.tag=[textField tag];
        [cityPicker setShowsSelectionIndicator:YES];
        year_txt.inputView = cityPicker;
        
        // Create done button in UIPickerView
        UIToolbar*  mypickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 56)];
        mypickerToolbar.barStyle = UIBarStyleDefault;
        [mypickerToolbar sizeToFit];
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        
        UIButton *selectButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
        [selectButton setTitle:@"Select" forState:UIControlStateNormal];
        [selectButton setShowsTouchWhenHighlighted:YES];
        [selectButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [selectButton addTarget:self action:@selector(pickerDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
        selectButton.tag=[textField tag];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithCustomView:selectButton];
        /*
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked:)];
         */
        [barItems addObject:doneBtn];
        //doneBtn.tag=[textField tag];
        [mypickerToolbar setItems:barItems animated:YES];
        year_txt.inputAccessoryView = mypickerToolbar;
        
        
    }

    return YES;
}

-(void)pickerDoneClicked:(id)sender
{
    
    switch ([sender tag]) {
        case 2://Year
        {
            if ([year_txt.text length]>0) {
                txt.userInteractionEnabled=YES;
                /*
                if ([datamanagerInstance checkDataFile:FILE_MAKE]) {
                    [[SingleTon instance] setMakedataarray:[datamanagerInstance readData:FILE_MAKE]];
                }
                else
                {
                 
                }
                 */
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                NSDictionary *dic=@{@"action" : @"getmake",@"myear" : year_txt.text};
                [carmodelDataInstance requestServerToGetCarData:dic :GET_CAR_MAKE_ACTION];
                
            }
            else
            {
                [[AppDelegate ShareDelegate] customAlert:@"Info!" withMessage:@"Please Select respective Year!!"];
            }
        }
            break;
        case 0://Make
        {
            if ([txt.text length]>0) {
                model_text.userInteractionEnabled=YES;
               
                /*
                if ([datamanagerInstance checkDataFile:File_MODEL]) {
                    [[SingleTon instance] setModeldataarray:[datamanagerInstance readData:File_MODEL]];
                }
                else
                {
                 
                }
                 */
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                NSDictionary *dic=@{@"action" : @"getmodel",@"myear" : year_txt.text,@"mmake" : txt.text};
                [carmodelDataInstance requestServerToGetCarData:dic :GET_CAR_RES_MODEL];
                
            }
            else
            {
                [[AppDelegate ShareDelegate] customAlert:@"Info!" withMessage:@"Please Select respective CarMake!!"];
            }
            
        }
            break;
            
        default:
            break;
    }
    
    NSLog(@"Done Clicked");
    [txt resignFirstResponder];
    [model_text resignFirstResponder];
    [year_txt resignFirstResponder];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView tag]==0)
    {
         return [[[SingleTon instance] makedataarray]count];
    }
    else if ([pickerView tag]==1)
    {
        return [[[SingleTon instance] modeldataarray]count];
    }
    else if ([pickerView tag]==2)
    {
        return [[[SingleTon instance] dateArray] count];
    }
    
    return 0;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView tag]==0)
    {
        return [[[SingleTon instance] makedataarray] objectAtIndex:row];
    }
    else if ([pickerView tag]==1)
    {
        return [[[SingleTon instance] modeldataarray]objectAtIndex:row];
    }
    else if ([pickerView tag]==2)
    {
        return [[[SingleTon instance] dateArray] objectAtIndex:row];
    }
    return 0;
}
- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView tag]==0)
    {
        txt.text = (NSString *)[[[SingleTon instance] makedataarray] objectAtIndex:row];
    }
    else if ([pickerView tag]==1)
    {
        model_text.text = (NSString *)[[[SingleTon instance] modeldataarray] objectAtIndex:row];
    }
    else if ([pickerView tag]==2)
    {
        year_txt.text =[[[SingleTon instance] dateArray] objectAtIndex:row];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)search_btn_action:(id)sender {
    
    if ([txt.text length]!=0&&[model_text.text length]!=0&&[year_txt.text length]!=0){
        
        [[SingleTon instance] setSearchStringAry:[[NSMutableArray alloc] initWithObjects:txt.text,model_text.text,year_txt.text,nil]];
        TabViewSearchResult *tab=[[TabViewSearchResult alloc] initWithNibName:@"TabViewSearchResult" bundle:nil];
        //[self.navigationController pushViewController:tab animated:YES];
        [self presentViewController:tab animated:YES completion:nil];
    }else{
        [[AppDelegate ShareDelegate] customAlert:@"Warning!" withMessage:@"Please Select car Make,Model and Year data."];
    }
}
-(void)dealloc
{
   txt.text=@"";
    model_text.text=@"";
    year_txt.text=@"";
}
#pragma -mark DelegateMethod
-(void)dataAfterRequest:(id)res:(int)type
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    switch (type) {
        case GET_YEAR_ACTION:
        {
            //[datamanagerInstance saveData:res :FILE_YEAR];
            [[SingleTon instance] setDateArray:res];
        }
            break;
        case GET_CAR_MAKE_ACTION:
        {
            
            
          //[datamanagerInstance saveData:res :FILE_MAKE];
          [[SingleTon instance] setMakedataarray:res];
        }
            break;
        case GET_CAR_RES_MODEL:
        {
           //[datamanagerInstance saveData:res :File_MODEL];
           [[SingleTon instance] setModeldataarray:res];
        }
            
            break;
        default:
            break;
    }
    
    

}
@end
