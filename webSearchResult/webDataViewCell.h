//
//  webDataViewCell.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 1/8/15.
//  Copyright (c) 2015 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface webDataViewCell : UITableViewCell
{
}
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *linkLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonOpenlink;




@end
