//
//  webResultViewController.m
//  AutoPedia
//
//  Created by Rasel_cse07 on 1/8/15.
//  Copyright (c) 2015 Rasel_cse07. All rights reserved.
//

#import "webResultViewController.h"
#import "webDataViewCell.h"

@interface webResultViewController ()

@end

@implementation webResultViewController
@synthesize datadic;
@synthesize webDataArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title=@"Expert Reviews";
        self.tabBarItem.image           = [[UIImage imageNamed:@"tab_googleN.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.tabBarItem.selectedImage   = [[UIImage imageNamed:@"tab_googleH.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    webDataArray=[[NSArray alloc] init];
   // [tableViewOutlet.layer setBorderWidth:2.00];
   // [tableViewOutlet.layer setBorderColor:[UIColor redColor].CGColor];
    tableViewOutlet.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-49);
    tableViewOutlet.contentInset = UIEdgeInsetsMake(44, 0, 0, 0);
    [self jsonTapped];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)jsonTapped
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *string = [NSString stringWithFormat:@"https://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=%@+%@+%@+Reviews&rsz=8",[[[SingleTon instance] searchStringAry] objectAtIndex:2],[[[SingleTon instance] searchStringAry] objectAtIndex:0],[[[SingleTon instance] searchStringAry] objectAtIndex:1]];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        self.datadic = (NSDictionary *)responseObject;
         webDataArray = [[datadic valueForKey:@"responseData"] valueForKey:@"results"];
         NSLog(@"dataArray--->%@",webDataArray);
        [tableViewOutlet reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];

    [operation start];
}

#pragma mark - Table View Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [webDataArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    webDataViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WebdataCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"webDataViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    cell.titleLable.text = [[webDataArray objectAtIndex:indexPath.row] valueForKey:@"titleNoFormatting"];
    cell.linkLabel.text = [[webDataArray objectAtIndex:indexPath.row] valueForKey:@"visibleUrl"];
    cell.detailTextLabel.text = [self formateString:[[webDataArray objectAtIndex:indexPath.row] valueForKey:@"content"]];
    [cell.buttonOpenlink addTarget:self action:@selector(openLinkAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonOpenlink setTag:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)table didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
- (IBAction)openLinkAction:(id)sender {
    NSString *urlstring = [NSString stringWithFormat:@"%@",[[webDataArray objectAtIndex:[sender tag]] valueForKey:@"url"]];
    web_ViewController *web;
    if ([UIScreen mainScreen].bounds.size.height!=568) {
        
        web=[[web_ViewController alloc] initWithNibName:@"web_ViewController_3" bundle:nil];
    }
    else
    {
        web=[[web_ViewController alloc] initWithNibName:@"web_ViewController" bundle:nil];
    }
    web.showDoneButton = YES;
    
    web.url=[NSURL URLWithString:urlstring];
    [self presentViewController:web animated:YES completion:nil];
}
-(NSString *)formateString:(NSString *)description
{
    NSString *first = [description stringByReplacingOccurrencesOfString:@"<b>"
                                                       withString:@""];
    NSString *formattedString = [first stringByReplacingOccurrencesOfString:@"</b>"
                                                                       withString:@""];
   // NSLog(@"%@", formattedString);
    return formattedString;
}
@end
