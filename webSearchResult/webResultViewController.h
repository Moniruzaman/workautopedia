//
//  webResultViewController.h
//  AutoPedia
//
//  Created by Rasel_cse07 on 1/8/15.
//  Copyright (c) 2015 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "web_ViewController.h"
#import "ParentViewConteoller.h"

@interface webResultViewController : ParentViewConteoller<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *tableViewOutlet;
}
@property(nonatomic,retain)NSDictionary *datadic;
@property(nonatomic,retain)NSArray *webDataArray;

@end
